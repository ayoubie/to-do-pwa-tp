
// Get all todos

export function fetchTodos() {
  const config = window.config;

  return fetch(`${config.api}/todos`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  })
  .then(result => result.json())
  .catch(error => {
    console.error(error);
    return false;
  })
}

// Get one todo by id

export function fetchTodo(id) {
  const config = window.config;

  return fetch(`${config.api}/todos/${id}`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  })
  .then(result => result.json())
  .catch(error => {
    console.error(error);
    return false;
  })
}

// Create a todo 
export function createTodo(data) {
  const config = window.config;
  return fetch(`${config.api}/todos`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  })
  .then(result => result.json())
  .catch(error => {
    console.error(error);
    return false;
  }); 
}

// Update a todo
export function updateTodo(data) {
  const config = window.config;

  return fetch(`${config.api}/todos/${data.id}`, {
    method: 'PUT',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(result => result.json())
    .catch(error => {
      console.error(error);
      return false;
    })
}

// Delete a todo
export async function deleteTodo(id) {
  const config = window.config;
  return fetch(`${config.api}/todos/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .then((result) => {result.json(); window.location.reload(true)})
  .catch(error => {
    console.error(error);
    return false;
  }); 
}